Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'art#index'
  resources :art
  get 'kanui', to: 'art#kanui'
end

